import { COLLECTIONS } from '../config/constants';
import { IContextData } from '../interfaces/contextData.interface';
import { asignDocumentId, findOneElement } from '../lib/db-operations';
import ResolversOperationsService from './resolvers-operations.service';
import  slugify from 'slugify';
class GenresService extends ResolversOperationsService {

  collection = COLLECTIONS.GENRES;

  constructor(root: object, variables: object, context: IContextData) {
    super(root, variables, context);
  }

  // Mostrar la lista de generos registrados en la base de datos
  async items() {
    const result = await this.list(this.collection, 'generos');

    return {
      status: result.status,
      message: result.message,
      genres: result.items,
    };
  }
  // Mostar detalles de un genero
  async details() {
    const result = await this.get(this.collection, 'genero');
    return {
      status: result.status,
      message: result.message,
      genre: result.item,
    };
  }
  // Crear un nuevo genero
  async insert() {
    // Comprobar que no esta en blanco ni es indefinido
    const genre = this.getVariables().genre;
    if (!this.checkData(genre || '')) {
      return {
        status: false,
        message: 'El genero no se ha especificado correctamente',
        genre: null,
      };
    }
    
    // Comprobar que no existe
    if (await this.checkInDatabase(genre || '')) {
      return {
        status: false,
        message: 'EL genero existe en la base de datos, intenta con otro',
        genre: null,
      };
    }
    // Generar el objeto que se guarara

    const genreObject = {
        id: await asignDocumentId(this.getDb(), this.collection, { id: -1} ),
        name: genre,
        slug: slugify(genre || '', { lower: true })
    };
    // Guardar el objeto creado anteriormente
    const result = await this.add(
      this.collection,
      genreObject,
      'genero'
    );

    return {
      status: result.status,
      message: result.message,
      genre: result.item,
    };
  }

 
  // Modificar item
  async modify(){
      const id = this.getVariables().id;
      const genre = this.getVariables().genre;
      // Comprobar que el id es correcto
      if(!this.checkData(String(id) || '')) {
        return {
            status: false,
            message: 'El ID del genero no se ha especificado correctamente',
            genre: null,
          };
      }
      // Comprobar que el genero es correcto
      if (!this.checkData(genre || '')) {
        return {
          status: false,
          message: 'El genero no se ha especificado correctamente',
          genre: null,
        };
      }
      // Comprobar que no existe
      if (await this.checkInDatabase(genre || '')) {
        return {
          status: false,
          message: 'EL genero existe en la base de datos, intenta con otro',
          genre: null,
        };
      }

      
      const objectUpdate = {
        name: genre,
        slug: slugify(genre || '', { lower: true }),
      };
      const result = await this.update(this.collection, { id }, objectUpdate, ' genero');
    return {
        status: result.status,
        message: result.message,
        genre: result.item,
      };
}
  // Eliminar item
async delete(){
    const id = this.getVariables().id;
    if(!this.checkData(String(id) || '')) {
        return {
            status: false,
            message: 'El ID del genero no se ha especificado correctamente',
            genre: null,
          };
      }
    const result = await this.del(this.collection, {id}, 'genero');
    return {
        status: result.status,
        message: result.message,
        genre: []
    };
}


   // Comprobar que no es nulo ni vacio
   private checkData(value: string) {
    return value === '' || undefined ? false : true;
  }

  // Comprobar que no es repetido
  private async checkInDatabase(value: string) {
    return await findOneElement(this.getDb(), this.collection, {
        
      name: value,
    });
  }
}

export default GenresService;
