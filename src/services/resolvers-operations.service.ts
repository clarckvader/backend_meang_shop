import { Db } from 'mongodb';
import { Icontext } from '../interfaces/context.interface';
import { IContextData } from '../interfaces/contextData.interface';
import { IVariables } from '../interfaces/variable.interface';
import { deleteOneElement, findElements, findOneElement, insertOneElement, updateOneElement } from './../lib/db-operations';

class ResolversOperationsService {
  private root: object;
  private variables: IVariables;
  private context: IContextData;

  constructor(root: object, variables: IVariables, context: IContextData) {
    this.root = root;
    this.variables = variables;
    this.context = context;
  }

  protected getDb(): Db {
    return this.context.db!;
  }
  protected getVariables(): IVariables {
    return this.variables;
  }
  protected getContext(): IContextData {
    return this.context;
  }

  



  // Listar iformacion
  protected async list(collection: string, listElement: string) {
    try {
      return {
        status: true,
        message: `Lista de ${listElement} cargada`,
        items: await findElements(this.getDb(), collection),
      };
    } catch (error) {
      return {
        status: false,
        message: `Error al cargar la lista de ${listElement}:${error}`,
        items: [],
      };
    }
  }
  // Obtener detalles del item
  protected async get(collection: string, listElement: string){
    const id = this.variables.id;
    
    try {
  
      return await findOneElement(this.getDb(), collection, { id }).then(
        result => {
          if (result) {
            return {
              status: true,
              message: `Informacion de el ${listElement} ${result.name} cargada correctamente`,
              item: result
            };
          }
          return {
            status: false,
            message: `Informacion del elemento de el ${listElement} con id ${ id } no fue cargada porque no existe`,
            item: null
          };
        }
      );
      
    } catch (error) {
      return {
        status: false,
        message: `Error al cargar la informacion de el elemento de el ${listElement}`,
        item: [],
      };
    }
  }
  // Anadir item
  protected async add(collection: string, document: object, item:string) {
    try {
      return await insertOneElement(this.getDb(), collection, document).then(
        res => {
          
          if (res.result.ok === 1) {
            return {
              status: true,
              message: `Elemento ${item} creado correctamente`,
              item: document
            };
          }
          return {
            status: false,
            message: `No se ha creado el elemento ${item}. Intentalo de nuevo`,
            item:null
          };
        }
      );
    } catch (error) {
      return {
        status: false,
        message: `Error inesperado al insertar el elemento ${item}. Intentalo de nuevo`,
        item: null
      };
    }
  }
  // Modificar item
  protected async update(collection: string, filter: object, objectUpdate:object, item: string){
    try {
      return await updateOneElement(
        this.getDb(),
        collection,
        filter,
        objectUpdate
      ).then(
        res => {
          if(res.result.nModified === 1 && res.result.ok){
            return {
              status: true,
              message:`Elemento del ${item} actualizado correctamente`,
              item: Object.assign({}, filter, objectUpdate)
            };
          }
          return {
            status: true,
            message:`Elemento del ${item} no se ha actualizado. Comprueba que estas filtrando correctamente`,
            item: Object.assign({}, filter, objectUpdate)
          };
        }
      );
    } catch (error) {
      return {
        status: false,
        message: `Error inesperado al insertar el elemento ${item}. Intentalo de nuevo`,
        item: null
      };
    }
  }
  // Eliminar item
  protected async del(collection: string, filter: object, item: string){
    try {
      return  await deleteOneElement(this.getDb(), collection, filter).then(
        res => {
          if (res.deletedCount === 1) {
            return {
              status: true,
              message: `El elemento ${item} ha sido eliminado correctamente`
            };
          }
          return {
            status: true,
            message: `El elemento ${item} no se ha eliminado, comprueba los filtros`
          };
        }
      );
    } catch (error) {
      return {
        status: false,
        message: `Error inesperado al eliminar el elemento ${item}. Intentalo de nuevo`,
        item: null
      };
    }
  }
}

export default ResolversOperationsService;
