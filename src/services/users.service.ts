import { COLLECTIONS, EXPIRETIME, MESSAGES } from '../config/constants';
import { IContextData } from '../interfaces/contextData.interface';
import { asignDocumentId, findOneElement} from '../lib/db-operations';
import ResolversOperationsService from './resolvers-operations.service';
import JWT from './../lib/jwt';
import bcrypt from 'bcrypt';

class UsersService extends ResolversOperationsService {
  private collection = COLLECTIONS.USERS;
  constructor(root: object, variables: object, context: IContextData) {
    super(root, variables, context);
  }
  // Mostrar la lista de usuarios
  async items() {
    const result = await this.list(this.collection, 'usuarios');

    return {
      status: result.status,
      message: result.message,
      users: result.items,
    };
  }

  async auth (){
    const info = new JWT().verify(this.getContext().token!);
    if (info === MESSAGES.TOKEN_VERIFICATION_FAILED){
      return{
        status: false,
        message:info,
        user: null
      };
    }
    return {
      status: true,
      message: 'Usuario autenticado correctamente mediante el token',
      user: Object.values(info)[0]
    };
  }

  // Inciar Sesion
  async login() {
    try {
      // Si el argumento(email de arriba) y la propiedad (email de abajo) coinciden en nombre, se lo escribe directamente
      // Caso contrario si el argumento se llamara por ejemplo, email value, abajo vendria email:emailvalue
      const variables = this.getVariables().user;
      const user = await findOneElement(this.getDb(), this.collection, {
        email: variables?.email,
      });
      
      if (user === null) {
        return {
          status: false,
          message: 'Password o usuario incorrecto',
          token: null,
        };
      }

      const passwordCheck = bcrypt.compareSync(
        variables?.password,
        user.password
      );
      

      if (passwordCheck !== false) {
        delete user._id;
        delete user.password;
        delete user.birthday;
        delete user.registerDate;
      }

      return {
        status: passwordCheck,
        message: !passwordCheck
          ? 'Password o usuario incorrecto'
          : 'Usuario autenticado correctamente',

        token: !passwordCheck ? null : new JWT().sign({ user }, EXPIRETIME.H24),
        user: !passwordCheck ? null : user,
      };
    } catch (error) {
      console.log(error);
      return {
        status: false,
        message: 'Error al cargar el usuario',
        token: null,
      };
    }
  }

  // Crear usuario
  async register(){
    const user = this.getVariables().user;
    
    // Comprobar que user no es null
    if(user === null) {
      return{
        status: false,
        message: 'Usuario no definido',
        user: null
      };
    }
    if(user?.password ===null ||
      user?.password===undefined||
      user?.password === ''){
        return{
          status: false,
          message: 'Usuario sin password correcto',
          user: null
        };
      }
    
    // Comprobar que el usuario no existe
       const userCheck = await findOneElement(this.getDb(), this.collection, {email: user?.email});
       if (userCheck !== null) {
           return{
               status: false,
               message: `El email ${user?.email} ya se encuentra registrado`,
               user: null
           };

       }
       // Validar email pero lo hare mas tarde :D
       // const validateEmail = new RegExp('/ab+c/', user.email);
       
      // Comprobar el ultimo usuario registrado para asignar ID
       user!.id = await asignDocumentId(this.getDb(), this.collection, { registerDate: -1});
      // Asignar la fecha en formato ISO en la propiedad registerDate
       user!.registerDate = new Date().toISOString();
       // Encriptar password
       user!.password = bcrypt.hashSync(user!.password, 10);
        // Guardar el documento (registro) en la coleccion
       const result = await this.add(this.collection, user || {}, 'ususario');
      return{
        status: result.status,
        message: result.message,
        user: result.item
      };
     
  }

  // Editar datos del usuario
  async modify(){
    const id = this.getVariables().id;
    const user = this.getVariables().user;
    const email = this.getVariables().user?.email;

    if(user === null) {
      return {
        status: false,
        message: 'El usuario es invalido',
        user: null,
      };
    }
    if(!this.checkData(String(id) || '')) {
      return {
          status: false,
          message: 'El ID del genero no se ha especificado correctamente',
          user: null,
        };
    }
   
    // Comprobar que no existe el email
    console.log(await this.checkInDatabase(email|| ''));
    if (await this.checkInDatabase(email|| '')) {
      return {
        status: false,
        message: 'EL email existe, prueba con otro',
        user: null,
      };
    }
    


    const result = await this.update(this.collection, { id }, user ||  {}, 'usario');
    return {
      status: result.status,
      message: result.message,
      user: result.item
    };
  }


  async delete () {
    const id = this.getVariables().id;
    if(!this.checkData(String(id) || '')) {
        return {
            status: false,
            message: 'El ID del usuario no se ha especificado correctamente',
            genre: null,
          };
      }
    const result = await this.del(this.collection, {id}, 'ususario');
    return {
        status: result.status,
        message: result.message,
        genre: []
    };

  }
   // Comprobar que no es nulo ni vacio
   private checkData(value: string) {
    return value === '' || undefined ? false : true;
  }

  // Comprobar que no es repetido
  private async checkInDatabase(value: string) {
    return await findOneElement(this.getDb(), this.collection, {
        
      email: value,
    });
  }
}

export default UsersService;
