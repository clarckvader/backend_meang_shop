import { Db } from 'mongodb';
import Database from './database';

/**
 * Obtener el Id que vamos a utilizar en el nuevo elemento
 * @param database Base de datos con la que estamos trabajando
 * @param collection Coleccion donde si busca el ultima elemento
 * @param sort Como queremos ordenarlo {<propiedad>elemento: -1}
 * 
 */

export const asignDocumentId = async (
  database: Db,
  collection: string,
  sort: object = { registerDate: -1 }
) => {
  const lastElement = await database
    .collection(collection)
    .find()
    .limit(1)
    .sort(sort)
    .toArray();
  
  if (lastElement.length === 0) {
    return '1';
  } else {
    return String(+lastElement[0].id + 1);
  }
};


/**
 * Encontrar un elemento en base al filtro 
 * @param database Base de datos con la que estamos trabajando
 * @param collection Coleccion donde se busca el elemento
 * @param filter parametro a buscar {<propiedad> {elemento}}
 */
export const findOneElement = async(
  database: Db,
  collection: string,
  filter: object
) =>{
  return database
          .collection(collection)
          .findOne(filter);
};

export const insertOneElement = async(
  database: Db,
  collection: string,
  document: object

)=>{
  return database
        .collection(collection)
        .insertOne(document);
};

export const insertManyElement = async(
  database: Db,
  collection: string,
  documents: object[]

)=>{
  return database
        .collection(collection)
        .insertMany(documents);
};

export const findElements = async (
  database: Db,
  collection: string,
  filter: object = {}
)=>{

  return await database.collection(collection).find(filter).toArray();
};
export const deleteOneElement = async (
  database: Db,
  collection: string,
  filter: object = {}
)=>{

  return await database.collection(collection).deleteOne(filter);
};

export const updateOneElement = async(
  database: Db,
  collection: string,
  filter: object,
  updateObject: object
) => {
  return await database.collection(collection).updateOne(filter, {$set: updateObject});
};


