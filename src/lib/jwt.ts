import { EXPIRETIME, MESSAGES, SECRET_KEY } from '../config/constants';
import jwt from 'jsonwebtoken';
import { IJwt } from '../interfaces/jwt.interface';

class JWT {
    private secretKey = SECRET_KEY as string;
    // informacion del payload con  tiempo de caducidad por defecto de 24 horas
    sign(data: IJwt, expiresIn: number = EXPIRETIME.H24) {
        return jwt.sign(
            {user: data.user},
            this.secretKey,
            {expiresIn} // 24 horas de caducidad
            
        );
    }

    verify(token: string){
        try {
            return jwt.verify(token, this.secretKey);
        } catch (e) {
            return MESSAGES.TOKEN_VERIFICATION_FAILED;
        }
    }
}

export default JWT;