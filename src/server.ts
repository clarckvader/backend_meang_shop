import { ApolloServer } from 'apollo-server-express';
import compression from 'compression';
import cors from 'cors';
import express from 'express';
import { createServer } from 'http';
import environment from './config/environments';
import schema from './schema';
import expressPlayground from 'graphql-playground-middleware-express';
import Database from './lib/database';
import { Icontext } from './interfaces/context.interface';

if (process.env.NODE_ENV !== 'production') {
  const env = environment;
  console.log(env);
}

// configuracion de las variables de entorno (lectura)

async function init() {
  const app = express();
  app.use(cors());

  app.use(compression());

  const database = new Database();

  const db = await database.init();

  

  const context = async({req, connection}: Icontext) =>{

    const token = (req) ? req.headers.authorization : connection.authorization;
    
    return {db, token};
  };

  
  const server = new ApolloServer({
    schema,
    introspection: true,
    context
    
  });

  server.applyMiddleware({app});

  app.get('/', expressPlayground({
    endpoint: './graphql'
  }));

  const httpServer = createServer(app);

  const PORT = process.env.PORT || 2002;

  httpServer.listen(
    {
      port: PORT,
    },
    () => console.log(`http://localhost:${PORT} API MEANG - online shop start`),
  );
}


init();
