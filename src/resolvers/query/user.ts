import { IResolvers } from 'graphql-tools';
import UsersService from '../../services/users.service';

const resolversUserQuery: IResolvers = {
  Query: {
    async users(_, __, { db }){
      return new UsersService(_, __, { db }).items();
   },
    async login(_, { email, password }, context) {
     return new UsersService(_, {user: {email, password}}, context).login();
    },
    me(_, __, { token }){
      return new UsersService(_, __, {token}).auth();
    }
  },
};

export default resolversUserQuery;
