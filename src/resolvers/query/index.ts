import GMR from 'graphql-merge-resolvers';
import resolversUserQuery from './user';
import resolversProductQuery from './product';
import resolversGenreQuery from './genre';

const queryResolvers = GMR.merge([
    resolversUserQuery,
    resolversProductQuery,
    resolversGenreQuery
]);


export default queryResolvers;