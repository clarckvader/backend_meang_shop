import { COLLECTIONS } from './../../config/constants';
import { IResolvers } from 'graphql-tools';
import UsersService from '../../services/users.service';

const resolversUserMutation: IResolvers = {
    Mutation: {
        async register(_, {user}, context){
         return new UsersService(_,{user}, context).register();
        },
        async updateUser(_, variables, context ){
            return new UsersService(_, variables , context).modify();
        },
        async deleteUser(_, variables, context){
            return new UsersService(_, variables, context).delete();
        }
    }
};

export default resolversUserMutation;