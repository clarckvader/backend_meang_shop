import { IResolvers } from 'graphql-tools';
import GenresService from '../../services/genres.service';

const resolversGenreMutation: IResolvers = {
    Mutation: {
        addGenre(_, variable, context) {
            return new GenresService(_, variable, context).insert();
        },
        updateGenre(_, variable, context) {
            return new GenresService(_, variable, context).modify();
        },
        deleteGenre(_, variable, context) {
            return new GenresService(_, variable, context).delete();
        }

    }
};

export default resolversGenreMutation;